
RELEASE_SUPPORT := $(shell dirname $(abspath $(firstword $(MAKEFILE_LIST))))/.make-release-support
SHELL=/bin/bash
VERSION=$(shell . $(RELEASE_SUPPORT) ; getVersion)
RELEASE_TAG=$(shell . $(RELEASE_SUPPORT) ; getRelease)
TAG=$(shell . $(RELEASE_SUPPORT); getTag)

OUTPUT ?= ./
COLLECTION_DIR := ./collections/ansible_collections/ska_cicd
COLLECTIONS ?= docker_base stack_cluster k8s
STAGE ?= test

.DEFAULT_GOAL := help

.PHONY: docker_base stack_cluster k8s

# define overides for above variables in here
-include PrivateRules.mak


# Fixed variables
TIMEOUT = 86400

# Docker and Gitlab CI variables
RDEBUG ?= ""
CI_ENVIRONMENT_SLUG ?= development
CI_PIPELINE_ID ?= pipeline$(shell tr -c -d '0123456789abcdefghijklmnopqrstuvwxyz' </dev/urandom | dd bs=8 count=1 2>/dev/null;echo)
CI_JOB_ID ?= job$(shell tr -c -d '0123456789abcdefghijklmnopqrstuvwxyz' </dev/urandom | dd bs=4 count=1 2>/dev/null;echo)
GITLAB_USER ?= ""
CI_BUILD_TOKEN ?= ""
REPOSITORY_TOKEN ?= ""
REGISTRY_TOKEN ?= ""
GITLAB_USER_EMAIL ?= "nobody@example.com"
DOCKER_VOLUMES ?= /var/run/docker.sock:/var/run/docker.sock
CI_APPLICATION_TAG ?= $(shell git rev-parse --verify --short=8 HEAD)
DOCKERFILE ?= Dockerfile
EXECUTOR ?= docker


vars:  ## Variables
	@echo "Current variable settings:"
	@echo "OUTPUT=$(OUTPUT)"
	@echo "COLLECTION_DIR=$(COLLECTION_DIR)"
	@echo "COLLECTIONS=$(COLLECTIONS)"
	# @echo "VERSION=$(VERSION)"
	# @echo "TAG=$(TAG)"
	# @echo "RELEASE_TAG=$(RELEASE_TAG)"

all: build ## call build

build: ## build all collections
	ansible-galaxy collection build --force --output-path=$(OUTPUT) \
	  $(COLLECTIONS) ;\
          for f in ska_cicd-*.tar.gz; do mv $$f $$(echo $$f | sed "s/ska_cicd-/ska_cicd_/")  ; done

test-ansible-publish:
	export COLLECTIONS
	@echo "COLLECTIONS=$(COLLECTIONS)"
	COLLECTIONS='$(COLLECTIONS)' bash /home/ubuntu/ska-k8s-tools/docker/deploy/scripts/ansible-publish.sh

clean: ## clean out build archives
	rm -f *.tar.gz

rebuild: clean build ## rebuild collections

lint: link ## lint check collections
	BASE=$$(pwd); \
	export ANSIBLE_COLLECTIONS_PATHS=$${BASE}/collections \
			ANSIBLE_COLLECTIONS_PATH=$${BASE}/collections \
			ANSIBLE_ROLES_PATH=$${BASE}/stack_cluster/roles:$${BASE}/docker_base/roles:$${BASE}/k8s/roles; \
	for i in $(COLLECTIONS); \
	do echo "Doing $${i} ..."; \
	ls ./$${i}/playbooks/*.yml; \
	ansible-lint --exclude=stack_cluster/roles/stackcluster/files ./$${i}/playbooks/*.yml; RC=$$?; \
	if [ $${RC} != 0 ]; then echo "ansible-lint error - ABORTING"; exit 1; fi; \
	ansible-lint --exclude=stack_cluster/roles/stackcluster/files ./$${i}/roles/*; RC=$$?; \
	if [ $${RC} != 0 ]; then echo "Roles lint error - ABORTING"; exit 1; fi; \
	flake8 ./$${i}; RC=$$?; \
	if [ $${RC} != 0 ]; then echo "flake8 error - ABORTING"; exit 1; fi; \
	cd $${BASE}; \
	done
	@echo "Linting PASSED!"

# link up all collections in the ansible collections path for
# ANSIBLE_COLLECTIONS_PATHS
link:
	mkdir -p $(COLLECTION_DIR)
	BASE=$$(pwd); cd $(COLLECTION_DIR); rm -f *; cd $${BASE}
	for i in $(COLLECTIONS); \
	do ln -s ../../../$${i} $(COLLECTION_DIR)/$${i}; \
	done

docker_base: link
	cd $@ && molecule test

stack_cluster: link
	cd $@ && molecule test

k8s: link
	cd $@ && molecule test

test: link  ## run collections tests
	BASE=$$(pwd); \
	mkdir -p $${BASE}/reports; \
	export ANSIBLE_COLLECTIONS_PATHS=$${BASE}/collections \
	       ANSIBLE_COLLECTIONS_PATH=$${BASE}/collections; \
	for i in $(COLLECTIONS); \
	do cd collections/ansible_collections/ska_cicd/$${i}; \
	printf "\n\n\nTesting: $${i}\n"; \
	pytest --junitxml=$${BASE}/reports/$${i}_junit-report.xml \
	       --log-file=$${BASE}/reports/$${i}_pytest-logs.txt -v; \
		   RC=$$?; cat $${BASE}/reports/$${i}_pytest-logs.txt; \
		   echo "RC: $${RC}"; \
	if [ $${RC} != 0 ]; then echo "Test error - ABORTING"; exit 1; fi; \
	cd $${BASE}; \
	done

rtest:  ## run make $(STAGE) using gitlab-runner
	if [ -n "$(RDEBUG)" ]; then DEBUG_LEVEL=debug; else DEBUG_LEVEL=warn; fi && \
	gitlab-runner --log-level $${DEBUG_LEVEL} exec $(EXECUTOR) \
    --docker-volumes  $(DOCKER_VOLUMES) \
    --docker-pull-policy always \
	--timeout $(TIMEOUT) \
		--env "GITLAB_USER=$(GITLAB_USER)" \
		--env "REGISTRY_TOKEN=$(REGISTRY_TOKEN)" \
		--env "CI_BUILD_TOKEN=$(CI_BUILD_TOKEN)" \
		--env "TRACE=1" \
		--env "DEBUG=1" \
	$(STAGE) || true

showver:
	@. $(RELEASE_SUPPORT); getVersion

update-patch-release: check-collection-variable ## update the patch version of the given COLLECTION i.e. make update-patch-release COLLECTION=docker_base
	$(eval ${COLLECTION}_VERSION := $(shell . $(RELEASE_SUPPORT); cd ${COLLECTION}; nextPatchLevel))
	make update-version ${COLLECTION}_VERSION=${${COLLECTION}_VERSION}

update-minor-release: check-collection-variable ## update the minor version of the given COLLECTION i.e. make update-minor-release COLLECTION=docker_base
	$(eval ${COLLECTION}_VERSION := $(shell . $(RELEASE_SUPPORT); cd ${COLLECTION}; nextMinorLevel))
	make update-version ${COLLECTION}_VERSION=${${COLLECTION}_VERSION}

update-major-release: check-collection-variable ## update the major version of the given COLLECTION i.e. make update-major-release COLLECTION=docker_base
	$(eval ${COLLECTION}_VERSION := $(shell . $(RELEASE_SUPPORT); cd ${COLLECTION}; nextMajorLevel))
	make update-version ${COLLECTION}_VERSION=${${COLLECTION}_VERSION}

update-version: check-status
	@echo "Working with collection: $(COLLECTION) ..."
	@echo ${${COLLECTION}_VERSION}
	@. $(RELEASE_SUPPORT) ; cd ${COLLECTION}; setRelease ${${COLLECTION}_VERSION}
	@echo "New version: ${${COLLECTION}_VERSION}"
	@echo "Changing tag from ${TAG} to ${COLLECTION}-${${COLLECTION}_VERSION}"
	@. $(RELEASE_SUPPORT) ; setTag ${COLLECTION}-${${COLLECTION}_VERSION}
	git add .release ${COLLECTION}/.release
	make release-collection
	echo "Update version of ${COLLECTION} to ${${COLLECTION}_VERSION}"
	git commit -m "Update version of ${COLLECTION} to ${${COLLECTION}_VERSION}"
	git tag ${COLLECTION}-${${COLLECTION}_VERSION} $$(git log -n 1 --format=%H .)
	@echo "Finished"

check-collection-variable:
	@if [ -z "${COLLECTION}" ]; then \
		echo "ERROR: Please provide a collection. eg: make update-release-x COLLECTION=k8s"; \
		exit 1; \
	fi
	@if [[ ! " ${COLLECTIONS} " =~ " ${COLLECTION} " ]]; then \
		echo "ERROR: Collection does not exist in the list of collections($(COLLECTIONS))"; \
		exit 1; \
	fi

check-status:
	@. $(RELEASE_SUPPORT) ; ! hasChanges || (echo "ERROR: there are still outstanding changes" >&2 && exit 1) ;

release-collection: setversion-collection build

setversion-collection: check-collection-variable
	BASE=$$(pwd); \
	RELEASE_TAG=$$(. $(RELEASE_SUPPORT) ; getRelease ${COLLECTION}/); \
	echo "Setting release: $${RELEASE_TAG} in $${BASE}/${COLLECTION}/galaxy.yml"; \
	sed -i.x -e "s/^version:.*/version: $${RELEASE_TAG}/" $${BASE}/${COLLECTION}/galaxy.yml; \
	git add $${BASE}/${COLLECTION}/galaxy.yml; \
	rm -f $${BASE}/${COLLECTION}/galaxy.yml.x

resource_test: link
	BASE=$$(pwd); \
	export ANSIBLE_COLLECTIONS_PATHS=$${BASE}/collections \
	       ANSIBLE_COLLECTIONS_PATH=$${BASE}/collections; \
	ansible-playbook -i inventory.sh $${ANSIBLE_COLLECTIONS_PATH}/ansible_collections/ska_cicd/k8s/playbooks/resources.yml --tags resources

pushtag:  ## push git tags
	git push
	git push --tags

help:  ## show this help.
	@echo "make targets:"
	@grep -E '^[0-9a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ": .*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'
	@echo ""; echo "make vars (+defaults):"
	@grep -E '^[0-9a-zA-Z_-]+ \?=.*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = " \\?= "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'

# when setting the release, use the following order:
# update the .release folder of the updated collection(s) and the root .release
# bump-patch-release, pushtag
