# ska-cicd-ansible-roles

Repository of common ansible role collections for the Systems Team.

## Summary

This repo contains a set of [Ansible Role Collections](https://docs.ansible.com/ansible/latest/user_guide/collections_using.html), that can be reused around Systems activities such as deploying variable OpenStack clusters, and standard application of tools such as Docker, ContainerD, Kubernetes.

## Collections

The current list of Collections are:

| Collection            | Roles                                                                              | Description                                                                                                                                                |
| --------------------- | ---------------------------------------------------------------------------------- | ---------------------------------------------------------------------------------------------------------------------------------------------------------- |
| docker_base   | containerd, <br> docker                                                            | deploy either docker and/or <br>containerd container engines                                                                                               |
| k8s           | charts, <br> haproxy, <br> helm, <br> join, <br> k8s, <br> kubectl, <br> resources, <br> metallb ,<br> binderhub, <br> ping | default SKA helm charts <br> haproxy Kubernetes LoadBalancer <br> helm client <br> join node to HA cluster <br> Kubernetes packages <br> Kubernetes client <br> Create Namespaces and Apply Limits and Quotas <br> Load balancer for kubernetes <br> Service to share Jupyter notebooks in the cloud <br> Ping service to test ingress |
| stack_cluster | common, <br> stackcluster                                                          | common VM setup <br> build variable OpenStack cluster with dynamic invetory                                                                                |

## Publishing

In order to run the publishing step, a git tag must be set.

## Making a release

When you are ready to publish a new version of one of the collections, you need to run `make update-x-release COLLECTION=<collection-name>` where `x` is either `patch`, `minor` or `major`.

So if you want to update the patch version of `systems_k8s` collection, you just run `make update-patch-release COLLECTION=systems_k8s`.

This will update the necessary version labels in `.release`, `galaxy.yml` files and will make a commit and tag it accordingly.

Finally, run `make pushtag`.  Once the CI job has completed (https://gitlab.com/ska-telescope/sdi/ska-cicd-roles/-/pipelines), make sure you trigger the manual step on the tag ref for `publish`.
