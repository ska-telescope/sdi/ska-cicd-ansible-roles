---
heat_template_version: 2017-09-01

description: >
  Heat stack containing a Nova instance.

parameters:
  name:
    type: string
    label: Instance name
  groupname:
    type: string
    label: Instance group name
  flavor:
    type: string
    label: Flavor name or UUID
  image:
    type: string
    label: Image name or UUID
  key_name:
    type: string
    label: SSH key pair name
  availability_zone:
    type: string
    label: Availability zone
    default: nova
  docker_vol_size:
    type: number
    label: Docker Volume size - GB
  data_vol_size:
    type: number
    label: Data Volume size - GB
  data_vol_snapshot:
    type: string
    label: Data Volume snapshot
  data_vol_name:
    type: string
    label: Data Volume name prefix
  create_data_vol:
    type: string
    label: Create data vol - list of applicable group names
  data2_vol_size:
    type: number
    label: Data2 Volume size - GB
  data2_vol_name:
    type: string
    label: Data2 Volume name prefix
  create_data2_vol:
    type: string
    label: Create data2 vol - list of applicable group names
  create_floating_ip:
    type: string
    label: Create floating IP - list of applicable group names
  cluster_net:
    type: json
    label: Network names and subnets to which the nodes should be attached
  secondary_net:
    type: json
    label: Network names and subnets to which the nodes should be attached
  secgroup_id:
    type: string
    label: Security Group ID
  num_nodes:
    type: number
    label: Number of instances

conditions:
  cond_fullname:
    equals:
    - get_param: num_nodes
    - 1
  cond_create_data_vol_snapshot:
    and:
    - contains: [{get_param: groupname}, {get_param: create_data_vol}]
    - not:
        equals:
        - get_param: data_vol_snapshot
        - ''
  cond_create_data_vol:
    and:
    - contains: [{get_param: groupname}, {get_param: create_data_vol}]
    - equals:
      - get_param: data_vol_snapshot
      - ''
  cond_create_data2_vol: {contains: [{get_param: groupname}, {get_param: create_data2_vol}]}
  cond_create_docker_vol: {not: {equals: [{get_param: docker_vol_size}, 0]}}

resources:
  instance_net:
    type: Cluster::NodeNet
    properties:
      cluster_net: { get_param: cluster_net }
      secgroup_id: { get_param: secgroup_id }
      groupname: { get_param: groupname }
      create_floating_ip: { get_param: create_floating_ip }
  # secondary_instance_net:
  #   type: Cluster::SecondaryNodeNet
  #   properties:
  #     secondary_net: { get_param: secondary_net }

  # node_config:
  #   type: OS::Heat::SoftwareConfig
  #   properties:
  #     config:
  #       str_replace:
  #         template: { get_file: node.cf }

  # node_init:
  #   type: OS::Heat::MultipartMime
  #   properties:
  #     parts:
  #       - config: { get_resource: node_config }

  instance:
    type: OS::Nova::Server
    properties:
      name: { if: [cond_fullname, get_param: groupname, get_param: name] }
      flavor: { get_param: flavor }
      image: { get_param: image }
      key_name: { get_param: key_name }
      # networks: { get_attr: [ instance_net, networks ] }
      networks:
        - port: { get_attr: [ instance_net, networks, 0, port ] }
        # - port: { get_attr: [ secondary_instance_net, networks, 0, port ] }
      availability_zone: { get_param: availability_zone }
      # user_data_format: RAW
      # user_data: { get_resource: node_init }

  docker_vol:
    type: OS::Cinder::Volume
    condition: cond_create_docker_vol
    properties:
      name:
        list_join:
        - '-'
        - [ { get_param: name },
            'docker_vol' ]
      size: { get_param: docker_vol_size }

  docker_vol_attachment:
    type: OS::Cinder::VolumeAttachment
    condition: cond_create_docker_vol
    properties:
      volume_id: { get_resource: docker_vol }
      instance_uuid: { get_resource: instance }
      mountpoint: /dev/vdb

# Create the data volume without using a snapshot
  data_vol:
    type: OS::Cinder::Volume
    condition: cond_create_data_vol
    properties:
      name:
        list_join:
        - '-'
        - [ { get_param: name },
            { get_param: data_vol_name } ]
      size: { get_param: data_vol_size }

  data_vol_attachment:
    type: OS::Cinder::VolumeAttachment
    condition: cond_create_data_vol
    properties:
      volume_id: { get_resource: data_vol }
      instance_uuid: { get_resource: instance }
      mountpoint: /dev/vdc

# Create the data volume with using a snapshot
  data_vol_snap:
    type: OS::Cinder::Volume
    condition: cond_create_data_vol_snapshot
    properties:
      name:
        list_join:
        - '-'
        - [ { get_param: name },
            { get_param: data_vol_name } ]
      size: { get_param: data_vol_size }
      snapshot_id: { get_param: data_vol_snapshot }

  data_vol_snap_attachment:
    type: OS::Cinder::VolumeAttachment
    condition: cond_create_data_vol_snapshot
    properties:
      volume_id: { get_resource: data_vol_snap }
      instance_uuid: { get_resource: instance }
      mountpoint: /dev/vdc

# snapshot_id: 5bd31723-88d7-43fa-81af-3a030323b334

  data2_vol:
    type: OS::Cinder::Volume
    condition: cond_create_data2_vol
    properties:
      name:
        list_join:
        - '-'
        - [ { get_param: name },
            { get_param: data2_vol_name } ]
      size: { get_param: data2_vol_size }

  data2_vol_attachment:
    type: OS::Cinder::VolumeAttachment
    condition: cond_create_data2_vol
    properties:
      volume_id: { get_resource: data2_vol }
      instance_uuid: { get_resource: instance }
      mountpoint: /dev/vdd

outputs:
  OS::stack_id:
    description: ID of the Nova instance
    value: { get_resource: instance }

  instance_data:
    description: ID of the primary IP
    value: { 'name': {get_param: name}, 'ip': {get_attr: [instance_net, primary_ip]}, 'floatingip': {get_attr: [instance_net, floating_ip]}, 'secondary_ip': {get_attr: [instance, networks, p3-bdn, 0]}, 'id': {get_resource: instance}, 'docker_vol': {if: [cond_create_docker_vol, { get_resource: docker_vol }, '']}, 'data_vol': {if: [cond_create_data_vol, { get_resource: data_vol }, '']}, 'data_vol_snap': {if: [cond_create_data_vol_snapshot, { get_resource: data_vol_snap }, '']}, 'data2_vol': {if: [cond_create_data2_vol, { get_resource: data2_vol }, '']} }

 #  secondary_instance_data:
 #    description: ID of the primary IP
 #    value: { 'name': {get_param: name}, 'ip': {get_attr: [instance, networks, p3-bdn, 0]}, 'id': {get_resource: instance} }
 #    # value: { 'name': {get_param: name}, 'ip': {get_attr: [secondary_instance_net, secondary_ip]}, 'id': {get_resource: instance} }

 # # # value: { get_attr: [instance, networks, 1, 0]}
