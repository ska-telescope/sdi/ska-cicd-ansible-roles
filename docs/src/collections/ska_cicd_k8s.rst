.. doctest-skip-all
.. collections-ska_cicd_k8s:

.. todo::
    - Insert todo's here

*************************
SKA CICD Kubernetes Roles
*************************

Ansible roles that handle installing and configuring HA Kubernetes, including
HAProxy LoadBalancer, multi-master/worker, helm, SKA core and common charts, and kubectl.

The role list can be found here at `k8s/roles <https://gitlab.com/ska-telescope/sdi/ska-cicd-ansible-roles>`_.
