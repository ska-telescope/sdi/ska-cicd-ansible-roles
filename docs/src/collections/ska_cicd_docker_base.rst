.. doctest-skip-all
.. collections-ska_cicd_docker_base:

.. todo::
    - Insert todo's here

*********************
SKA CICD Docker Roles
*********************

Ansible roles that handle installing and configuring Docker and ContainerD.

The role list can be found here at `docker_base/roles <https://gitlab.com/ska-telescope/sdi/ska-cicd-ansible-roles>`_.
