.. doctest-skip-all
.. collections-ska_cicd_stack_cluster:

.. todo::
    - Insert todo's here

********************************
SKA CICD OpenStack Cluster Roles
********************************

Ansible roles that handle building flexible OpenStack node clusters, including dynamic inventory.

The role list can be found here at `stack_cluster/roles <https://gitlab.com/ska-telescope/sdi/ska-cicd-ansible-roles>`_.
