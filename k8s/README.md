# Ansible Collection - ska.systems_k8s

Documentation for the collection.

## Roles

TODO: Document other roles
...

### Resources

Creates new namespaces in the cluster created by charts role and applies limits resource quotas according to a helm chart values.

**How to use**
If `chart_url` variable is defined, then the values are extracted from the helm chart from the url.
`chart_url` can be either a git repository or a packaged helm chart.

If the Helm chart is a git repository then `chart_dir` (top-level folder of the chart) should be defined to define where to search for the helm chart.

Then, the helm chart is templated using `--dependency-update` flag set to cover dependencies as well.

Total values of:

- CPU requests
- CPU limits
- Memory Requests
- Memory Limits
- Ephemeral Storage Requests
- Ephemeral Storage Limits

for Kubernetes resources of:

- Deployment
- StatefulSet
- Pod
- DaemonSet
- Job

are extracted and a `csv file`(named resources.csv) for all the apps are created in the directory of the playbook with a `json file`(named resources.json) defining total values unless `current_dir` is defined.

You can also use this role with only `extractvalues` tag (i.e. `ansible-playbook resources.yml --tags extractvalues`) so that only the `csv` and `json` files are created without creating namespaces and applying quotas.

**Variables:**

- `chart_url` and `chart_dir`: Used as described above to define the Helm Chart
- `charts_namespaces`: Namespaces to be created
- `charts_quotas_*`: Default total values of resources to be applied to the namespaces. These values are overridden if a chart is defined with above variables.
- `memory_format`: Memory size format. Set this true if you want csv file memory format to be human readable. It is false by default to enable easy calculations on values
