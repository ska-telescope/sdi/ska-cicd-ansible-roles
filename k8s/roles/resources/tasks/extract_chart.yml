- name: Extract values from Helm Chart
  block:
    - name: Define copy directory for the csv file
      block:
        - name: Get current path
          command: pwd
          register: current_dir_var
          changed_when: false

        - name: Seth current dir
          set_fact:
            current_dir: "{{ current_dir_var.stdout }}"
      when: current_dir is not defined

    - name: Print current dir
      debug:
        var: current_dir

    - name: Create temp directory
      tempfile:
        state: directory
        suffix: -helm-chart
      register: temp_dir

    - name: Print temp dir
      debug:
        var: temp_dir

    - name: Extract resource quotas from helm chart (git repo)
      block:
        - name: Download the chart git repo
          git:
            repo: "{{ chart_url }}"
            dest: "{{ temp_dir.path }}"
            version: "{{ chart_version|default('master') }}"

        - name: Copy script file
          copy:
            src: parse_resources.py
            dest: "{{ temp_dir.path }}"
            mode: "0755"

        - name: Extract the resources from git based chart
          shell: helm template {{ chart_dir }} --dependency-update | python3 parse_resources.py
          args:
            chdir: "{{ temp_dir.path }}"
          when: not memory_format

        - name: Extract the resources from git based chart
          shell: helm template {{ chart_dir }} --dependency-update | python3 parse_resources.py --for-humans
          args:
            chdir: "{{ temp_dir.path }}"
          when: memory_format

      when: chart_dir is defined

    - name: Extract resource quotas from helm chart (from url)
      block:
        - name: create temp chart directory
          file:
            path: "{{ temp_dir.path }}"
            state: directory
            mode: "0755"

        - name: Copy script file
          copy:
            src: parse_resources.py
            dest: "{{ temp_dir.path }}"
            mode: "0755"

        - name: Download the chart file from url
          get_url:
            url: "{{ chart_url }}"
            dest: "{{ temp_dir.path }}"
            mode: "0755"
          register: chart_file

        - name: Extract the resources from chart file
          shell: helm template {{ chart_file.dest }} --dependency-update | python3 parse_resources.py
          args:
            chdir: "{{ temp_dir.path }}"
          when: not memory_format

        - name: Extract the resources from chart file
          shell: helm template {{ chart_file.dest }} --dependency-update | python3 parse_resources.py --for-humans
          args:
            chdir: "{{ temp_dir.path }}"
          when: memory_format

      when: chart_dir is not defined

    - name: Copy csv file to current_dir
      copy:
        src: "{{ temp_dir.path }}/resources.csv"
        remote_src: true
        dest: "{{ current_dir }}"
        mode: "0644"

    - name: Copy json file to current_dir
      copy:
        src: "{{ temp_dir.path }}/resources.json"
        remote_src: true
        dest: "{{ current_dir }}"
        mode: "0644"

    - name: Get resources.json file
      slurp:
        src: "{{ temp_dir.path }}/resources.json"
      register: resources_file

    - name: Create total resources variable
      set_fact:
        total_resources: "{{ resources_file['content'] | b64decode | from_json }}"

    - name: Print total values
      debug:
        var: total_resources

    - name: Override variables with values obtained from helm chart
      set_fact:
        charts_quotas_cpu_requests: "{{ total_resources['total_cpu_requests'] }}"
        charts_quotas_cpu_limits: "{{ total_resources['total_cpu_limits'] }}"

        charts_quotas_memory_requests: "{{ total_resources['total_memory_requests'] }}"
        charts_quotas_memory_limits: "{{ total_resources['total_memory_limits'] }}"

        charts_quotas_ephemeral_storage_requests: "{{ total_resources['total_ephemeral_storage_requests'] }}"
        charts_quotas_ephemeral_storage_limits: "{{ total_resources['total_ephemeral_storage_limits'] }}"

  always:
    - name: Delete tmp chart folder
      file:
        path: "{{ temp_dir.path }}"
        state: absent

  delegate_to: 127.0.0.1
  when: chart_url is defined
